<?php

class JsHelper
{

    /**
     * Returns
     * @return array
     */
    public static function getScriptVariables(): array
    {
        global $globalScripts;

        return array_merge($globalScripts ?? [], self::getJsConfigVars());
    }

    /**
     * @param array $items
     * @return void
     */
    public static function addGlobalScripts(array $items)
    {
        global $globalScripts;

        if (empty($globalScripts)) {
            $globalScripts = $items;
        } else {
            foreach ($items as $key => $item) {
                $globalScripts[$key] = $item;
            }
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Javascript window config
    |--------------------------------------------------------------------------
    |
    | Adding values to this array will give JavaScript global
    | keys available for all the client side application.
    | The path to each key is found in window[namespace]
    |
    */
    /** @return array */
    public static function getJsConfigVars(): array
    {
        /**@var WP_Rewrite $wp_rewrite */
        global $wp_rewrite;
        return [
            'site' => [
                'lang' => get_locale(),
                'name' => get_bloginfo('name'),
                'logo' => has_custom_logo() ? get_custom_logo() : get_bloginfo('name'),
                'home_is_page' => (get_option('show_on_front') === "page"),
                'home_page_id' => get_option('page_on_front')
            ],
            'permalink_structure' => [
                'page' => self::convertWildcard($wp_rewrite->get_page_permastruct()),
                'post' => self::convertWildcard($wp_rewrite->permalink_structure)
            ],
            'formats' => [
                'time' => self::convertDateTimeString(get_option('time_format')),
                'date' => self::convertDateTimeString(get_option('date_format')),
                'datetime' => self::convertDateTimeString(get_option('links_updated_date_format'))
            ],
            'routes' => [
                'menus' => '/wp-json/wp-api-menus/v2/menus',
                'pages' => '/wp-json/wp/v2/pages',
                'posts' => '/wp-json/wp/v2/posts'
            ]
        ];
    }

    public static function convertDateTimeString($string)
    {

        // Explode into array
        $matches = preg_split("/([\s,:])/", $string, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

        //Define replacements
        $replacements = [
            // Day of month
            "d" => "DD", //	01–31
            "j" => "D", // 1–31
            "S" => "o", // 	st, nd or th in the 1st, 2nd or 15th.

            // Weekday
            "l" => "dddd", // Sunday – Saturday
            "D" => "ddd", // Mon – Sun

            // Month
            "m" => "MM", // 01–12
            "n" => "M", // 1–12
            "F" => "MMMM", // January – December
            "M" => "MMM", // Jan - Dec

            // Year
            "Y" => "YYYY", // 1999, 2003
            "y" => "YY", // 99, 03

            // Time
            "a" => "a", // am, pm
            "A" => "A", // AM, PM
            "g" => "h", // 1–12
            "h" => "hh", // 01–12
            "G" => "H", // 0-23
            "H" => "HH", // 00-23
            "i" => "mm", // 00-59
            "s" => "ss", // 00-59
            "T" => "z", // EST, MDT ...

            // Full Date/Time
            "c" => "YYYY-MM-DDTHH:mm:ssZ", // ISO 8601
            "r" => "ddd, DD MMM YYYY HH:mm:ss ZZ", // RFC 2822
            "U" => "X" // Unix timestamp
        ];

        foreach ($matches as $match) {
            if (!empty($replacements[$match])) {
                $matches = str_replace($match, $replacements[$match], $matches);
            }
        }


        return implode($matches);
    }

    public static function convertWildcard($string): string
    {
        // Explode into array
        $matches = explode("/", $string);

        // Remove empty values
        $matches = array_filter($matches);

        $replacements = [
            "%pagename%" => ":pagename([a-z-\d]+)", // Page slug
            "%postname%" => ":postname([a-z-\d]+)", //The post slug of your post
            "%post_id%" => ":post_id(\\d+)", //The unique ID of a post
            "%category%" => ":category([a-z-]+)", //The category a post was assigned to
            "%year%" => ":year(\\d{4})", //The year the article was published
            "%monthnum%" => ":month(\\d{2})", //The month the article was published
            "%day%" => ":day(\\d{2})", //The day the article was published
        ];

        foreach ($matches as $match) {
            if (!empty($replacements[$match])) {
                $matches = str_replace($match, $replacements[$match], $matches);
            }
        }

        // Glue it back together
        return "/" . implode("/", $matches);
    }

}