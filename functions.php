<?php

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );

show_admin_bar(false);

$files = [
    "/app/JsHelper.php",
    "/app/Api/Menu.php"
];

foreach($files as $file) {
    require_once(dirname(__FILE__). $file);
}