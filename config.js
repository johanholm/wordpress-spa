module.exports = {
    // The Process env in set in the package.json "scripts" object
    inProduction : (process.env.NODE_ENV === "production"),
    useSourceMaps: !(process.env.NODE_ENV === "production"),

    // Output options
    outputDir: "./dist",
    outputFilename: "app",

    // The files to watch when compiling
    watchFiles: [
        "./resources/js/app.js",
        "./resources/sass/app.scss"
    ],

    // Plugin options
    autoprefixer: "", // "last 4 versions"
};