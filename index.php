<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_bloginfo('name'); ?></title>
    <meta name="description" content="<?php echo get_bloginfo("description"); ?>">
    <script>
        window.app = <?php echo json_encode(JsHelper::getScriptVariables()); ?>
    </script>
    <!-- Styles -->
    <link href="<?php echo get_stylesheet_directory_uri() ?>/dist/app.css" rel="stylesheet">
</head>
<body>

<div id="app">
    <div class="container">
        <navigation id="2"></navigation>
        <main>
            <transition name="fade" mode="out-in">
                <router-view :key="$route.path"></router-view>
            </transition>
        </main>

    </div>
</div>

<!-- Scripts -->
<script src="<?php echo get_stylesheet_directory_uri() ?>/dist/app.js"></script>
</body>
</html>