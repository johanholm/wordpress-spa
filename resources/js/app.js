import Vue from 'vue';
import VueRouter from 'vue-router';

// Register mixins
import * as mixinMethods from './mixins';
Vue.mixin({
    methods: mixinMethods,
});

// Setup router
Vue.use(VueRouter);
import routes from './routes';

const router = new VueRouter({
    routes: routes,
    // mode: 'history',
});

import Navigation from './components/Navigation.vue';

// Setup Vue VM
new Vue({
    el: "#app",
    router: router,
    components: {
        navigation: Navigation
    }
});
