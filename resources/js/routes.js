import Home from './pages/Home.vue';
import Page from './pages/Page.vue';
import Post from './pages/Post.vue';

export default [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: window.app.permalink_structure.page,
        name: 'page',
        component: Page
    },
    {
        path: window.app.permalink_structure.post,
        name: 'post',
        component: Post
    },
    {
        path: '*',
        redirect: { name: 'home' }
    }
];