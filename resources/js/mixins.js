import moment from 'moment';
moment.locale(window.app.site.lang);

export function formatDateTime(dt) {
    return moment.parseZone(dt).format(window.app.formats.datetime);
}

export function formatTime(time) {
    return moment.parseZone(time).format(window.app.formats.time);
}

export function formatDate(date) {
    return moment.parseZone(date).format(window.app.formats.date);
}

export function linkTo(url) {
    return new URL(url).pathname;
};

export function currentMetaPage() {
    let query = location.search.match(/page=(\d+)/);
    return query ? query[1] : 1;
}